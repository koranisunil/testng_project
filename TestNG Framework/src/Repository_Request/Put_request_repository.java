package Repository_Request;

import java.io.IOException;
import java.util.ArrayList;

import Utility_common_methods.Excel_data_extractor;

public class Put_request_repository {
	public static String Put_request_TC1() throws IOException {
		
		ArrayList<String> Data = Excel_data_extractor.Excel_data_reader("Test_data", "Put_API", "Put_TC1");
		//System.out.println(Data);
		String name = Data.get(1);
		String job = Data.get(2);
		String requestBody = "{\n" + "    \"name\": \""+name+"\",\n" + "    \"job\": \""+job+"\"\n" + "}";
		return requestBody;
	}

}
