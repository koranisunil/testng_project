package Repository_Request;

import org.testng.annotations.DataProvider;

public class Data_driver_TestNG {
	@DataProvider()
	public Object[][]Post_requestBody(){
		return new Object[][] {
			{"Sunil","QA"},
			{"Manisha","SrQA"},
			{"Nikhil","JrQA"}
		};
	}
}
