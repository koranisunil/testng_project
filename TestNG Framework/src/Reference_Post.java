
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

import org.testng.Assert;

public class Reference_Post {

	public static void main(String[] args) {

		// Step 1 Declare base URL
		RestAssured.baseURI = "https://reqres.in";

		// Step 2 Configure the request parameters & Trigger the API

		String requestBody = "{\n" + "    \"name\": \"morpheus\",\n" + "    \"job\": \"leader\"\n" + "}";
		String responseBody = given().header("Content-Type", "application/json")
				.body(requestBody)
				.when().post("/api/users")
				.then().extract().response().asString();
		System.out.println("ResponseBody is :" + responseBody);

		// Step 3 Create an Object of Json Path to Parse the requestBody & responseBody

		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String expected_date = currentdate.toString().substring(0, 11);
		JsonPath jsp_res = new JsonPath(responseBody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_id = jsp_res.getString("id");
		String res_createdAt = jsp_res.getString("createdAt");
		res_createdAt = res_createdAt.substring(0, 11);

		// Step 4 Validate response Body
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createdAt, expected_date);

	}

}
