package TestNG_Dataprovider;

import java.io.File;
import java.io.IOException;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import API_common_methods.Common_method_handle_API;
import Endpoints.Post_endpoint;
import Parse_req.Post_Reqbody;
import Repository_Request.Data_driver_TestNG;
import Repository_Request.Post_request_repository;
import Test_package.Post_TC1;
import Utility_common_methods.Handle_API_logs;
import Utility_common_methods.Handle_directory;

public class Data_Driven_diff_class extends Common_method_handle_API {

	static File log_dir;
	static String requestBody;
	static String endpoint;
	static String responseBody;
	
	@BeforeTest
	public static void Test_Setup() throws IOException {
		log_dir = Handle_directory.Create_log_directory("Post_TC1_logs");
		requestBody = Post_request_repository.Post_request_TC1();
		endpoint = Post_endpoint.Post_endpoint_TC1();
	}

	@Test(dataProvider="Post_requestBody",dataProviderClass=Data_driver_TestNG.class)
	public static void Post_executor(String Name, String Job) throws IOException {
		requestBody= "{\r\n" + "    \"name\": \""+Name+"\",\r\n"
                + "    \"job\": \""+Job+"\"\r\n"
		          + "}";
		for (int i = 0; i < 5; i++) {

			int statusCode = Post_statusCode(requestBody, endpoint);
			System.out.println(statusCode);
			if (statusCode == 201) {

				responseBody = Post_responseBody(requestBody, endpoint);
				System.out.println(responseBody);
				Post_Reqbody.validator(requestBody, responseBody);
				break;

			} else {
				System.out.println("Expected Status code not found hence retrying");
			}
		}
	}

	@AfterTest
	public static void Test_Teardown() throws IOException {
		String Test_Class_Name=Post_TC1.class.getName();
		Handle_API_logs.evidence_creator(log_dir, Test_Class_Name, endpoint, requestBody, responseBody);
	}
}
