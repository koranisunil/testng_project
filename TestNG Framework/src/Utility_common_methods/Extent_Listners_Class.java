package Utility_common_methods;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class Extent_Listners_Class implements ITestListener {

	ExtentSparkReporter SparkReport;
	ExtentReports ExtentReport;
	ExtentTest Test;
	
	public void ReportConfiguration() {
		SparkReport = new ExtentSparkReporter(".//extent-Report//Report.html");
		ExtentReport = new ExtentReports();
		ExtentReport.attachReporter(SparkReport);
		
		ExtentReport.setSystemInfo("OS", "Mac OS");
		ExtentReport.setSystemInfo("User", "Sunil Korani");
		
		SparkReport.config().setDocumentTitle("My first Project Report");
		SparkReport.config().setReportName("Sprint One Report");
		SparkReport.config().setTheme(Theme.STANDARD);
	}
	
	@Override
	public void onStart(ITestContext context) {
		System.out.println("Start Method Invoked");
		ReportConfiguration();
	}
	
	@Override
	public void onFinish(ITestContext context) {
		System.out.println("On Finish Method Invoked");
		ExtentReport.flush();
	}
	
	@Override
	public void onTestFailure(ITestResult result) {
		System.out.println("Name of Failed Method" +result.getName());
		Test = ExtentReport.createTest(result.getName());
		Test.log(Status.FAIL, MarkupHelper.createLabel("Name of Testcase Failed" +result.getName(), ExtentColor.RED));
	}
	
	@Override
	public void onTestSkipped(ITestResult result) {
		System.out.println("Name of Method Skipped" +result.getName());
		Test = ExtentReport.createTest(result.getName());
		Test.log(Status.SKIP, MarkupHelper.createLabel("Name of Testcase Skipped" +result.getName(), ExtentColor.YELLOW));
	}
	
	@Override
	public void onTestStart(ITestResult result) {
		System.out.println("On Test Start: " +result.getName());
	}
	
	@Override
	public void onTestSuccess(ITestResult result) {
		System.out.println("Name of Method Passed" +result.getName());
		Test = ExtentReport.createTest(result.getName());
		Test.log(Status.PASS, MarkupHelper.createLabel("Name of Testcase Passed" +result.getName(), ExtentColor.GREEN));
	}	
}
