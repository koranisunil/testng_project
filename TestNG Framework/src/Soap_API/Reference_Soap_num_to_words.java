package Soap_API;

import io.restassured.RestAssured;
import io.restassured.path.xml.XmlPath;

import static io.restassured.RestAssured.given;

import org.testng.Assert;;
public class Reference_Soap_num_to_words {

	public static void main(String[] args) {
		RestAssured.baseURI = "https://www.dataaccess.com";
		
		String requestBody = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n"
				+ "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\r\n"
				+ "  <soap:Body>\r\n"
				+ "    <NumberToWords xmlns=\"http://www.dataaccess.com/webservicesserver/\">\r\n"
				+ "      <ubiNum>500</ubiNum>\r\n"
				+ "    </NumberToWords>\r\n"
				+ "  </soap:Body>\r\n"
				+ "</soap:Envelope>";
		
		String responseBody = given().header("Content-Type","text/xml; charset=utf-8")
				.body(requestBody)
				.when().post("webservicesserver/NumberConversion.wso")
				.then().extract().response().getBody().asString();
		System.out.println(responseBody);
		
		XmlPath xml_res = new XmlPath(responseBody);
		String res_tag = xml_res.getString("NumberToWordsResult");
		System.out.println(res_tag);
		
		Assert.assertEquals(res_tag, "five hundred ");
				

	}

}
