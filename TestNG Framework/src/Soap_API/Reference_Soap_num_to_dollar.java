package Soap_API;

import io.restassured.RestAssured;
import io.restassured.path.xml.XmlPath;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

public class Reference_Soap_num_to_dollar {

	public static void main(String[] args) {
	
		// Step 1 Declare the Base URL
		
		RestAssured.baseURI= "https://www.dataaccess.com";
		
		// Step 2 Declare the requestBody
		
		String requestBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://www.dataaccess.com/webservicesserver/\">\n"
				+ "   <soapenv:Header/>\n"
				+ "   <soapenv:Body>\n"
				+ "      <web:NumberToDollars>\n"
				+ "         <web:dNum>45</web:dNum>\n"
				+ "      </web:NumberToDollars>\n"
				+ "   </soapenv:Body>\n"
				+ "</soapenv:Envelope>";
		
		// Step 3 Trigger the API & Fetch the responseBody
		String responseBody = given().header("Content-Type", "text/xml; charset=utf-8")
				.body(requestBody)
				.when().post("webservicesserver/NumberConversion.wso")
				.then().extract().response().getBody().asString();
		
		// Step 4 Print the responseBody
		System.out.println(responseBody);
		
		// Step 5 Extract the responseBody parameters
		XmlPath xml_res = new XmlPath(responseBody);
		String res_tag = xml_res.getString("NumberToDollarsResult");
		System.out.println(res_tag);
		
		// Validate the responseBody
		Assert.assertEquals(res_tag, "forty five dollars");
		
		
	}

}
