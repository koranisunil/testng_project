package Test_package;

import java.io.File;
import java.io.IOException;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import API_common_methods.Common_method_handle_API;
import Endpoints.Put_endpoint;
import Parse_req.Put_ReqBody;
import Repository_Request.Put_request_repository;
import Utility_common_methods.Handle_API_logs;
import Utility_common_methods.Handle_directory;

public class Put_TC1 extends Common_method_handle_API {
	static File log_dir;
	static String requestBody;
	static String endpoint;
	static String responseBody;
	
	@BeforeTest
	public static void Test_Setup() throws IOException {
		System.out.println("@BeforeTest");
		log_dir = Handle_directory.Create_log_directory("Put_TC1_logs");
		requestBody = Put_request_repository.Put_request_TC1();
		endpoint = Put_endpoint.Put_endpoint_TC1();
	}

	@Test
	public static void Put_executor() throws IOException {
		System.out.println("@Test_Put_executor");
		for (int i = 0; i < 5; i++) {
			int statusCode = Put_statusCode(requestBody, endpoint);
			System.out.println(statusCode);
			if (statusCode == 200) {

				responseBody = Put_responseBody(requestBody, endpoint);
				System.out.println(responseBody);
				Put_ReqBody.validator(requestBody, responseBody);
				break;

			} else {
				System.out.println("Expected Status code not found hence retrying");
			}
		}
	}
	
	@AfterTest
	public static void Test_Teardown() throws IOException {
		System.out.println("@AfterTest");
		String Test_Class_Name=Put_TC1.class.getName();
		Handle_API_logs.evidence_creator(log_dir, Test_Class_Name, endpoint, requestBody, responseBody);
	}
}
