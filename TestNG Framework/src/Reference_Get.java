import io.restassured.RestAssured;
import static io.restassured.RestAssured.given;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;

public class Reference_Get {

	public static void main(String[] args) {
		
		int exp_id[]= {7, 8, 9, 10, 11, 12};
		String exp_first_name[]= {"Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel"};
		String exp_last_name[]= {"Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell"};
		String exp_email[]= {"michael.lawson@reqres.in", "lindsay.ferguson@reqres.in",
				"tobias.funke@reqres.in", "byron.fields@reqres.in", 
				"george.edwards@reqres.in", "rachel.howell@reqres.in"};
		
		RestAssured.baseURI = "https://reqres.in";
		
		String responseBody = given()
				.when().get("api/users?page=2")
				.then().extract().response().asString();
		
		System.out.println("ResponseBody is :" +responseBody);
		
		JSONObject array_res = new JSONObject(responseBody);
		JSONArray dataarray = array_res.getJSONArray("data");
		System.out.println(dataarray);
		int count = dataarray.length();
		System.out.println(count);
		for (int i=0; i<count; i++) {
			int res_id = dataarray.getJSONObject(i).getInt("id");
			String res_first_name = dataarray.getJSONObject(i).getString("First_name");
			String res_last_name = dataarray.getJSONObject(i).getString("last_name");
			String res_email = dataarray.getJSONObject(i).getString("email");
			
			Assert.assertEquals(res_id, exp_id[i]);
			Assert.assertEquals(res_first_name, exp_first_name[i]);
			Assert.assertEquals(res_last_name, exp_last_name[i]);
			Assert.assertEquals(res_email, exp_email[i]);
		}
		
	}

}
